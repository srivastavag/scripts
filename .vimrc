set nocompatible
filetype indent plugin on
syntax on
set cursorline
set cursorcolumn
set hidden
set wildmenu
set showcmd
set hlsearch
set incsearch
set ignorecase
set smartcase
set backspace=indent,eol,start
set autoindent
set nostartofline
set ruler
set laststatus=2
set confirm
set visualbell
set t_vb=
set cmdheight=2
set notimeout ttimeout ttimeoutlen=200
set pastetoggle=<F11>
set number
map Y y$
nnoremap <C-L> :nohl<CR><C-L>
colorscheme default
filetype indent on
set scrolloff=10
" Plugin to highlight the trailing spaces
highlight RedundantWhitespace ctermbg=red guibg=red
match RedundantWhitespace /\s\+$\| \+\ze\t/

highlight OverLength ctermbg=red ctermfg=white guibg=#592929
match OverLength /\%121v.\+/

"set guifont=Monaco:h80

" Code folding
set foldenable
set foldlevelstart=10
set foldnestmax=10
set foldmethod=indent


:if &diff
 "map n ]c
  " map N [c
   "map = :diffget^M
  colorscheme evening
  "colorscheme mycolorscheme
  "highlight DiffAdd    cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
  "highlight DiffDelete cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
  "highlight DiffChange cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
  "highlight DiffText   cterm=bold ctermfg=10 ctermbg=88 gui=none guifg=bg guibg=Red
:endif

"pecific settings for Python files
autocmd FileType python setlocal expandtab shiftwidth=4 softtabstop=4 tabstop=4
set tabstop=4
set ignorecase

